//
//  ScheduleViewModel.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

protocol ScheduleDelegate {
    func onScheduleModelUpdated()
}

class SchedulesViewModel {
    
    // Variables
    var schedulesViewModel: [ScheduleViewModel] = []
    private var _delegate: ScheduleDelegate?
    var numberOfSchedules: Int {
        return schedulesViewModel.count
    }
    
    // Functions
    init(delegate: ScheduleDelegate) {
        self._delegate = delegate
    }

    func deleteSchedule(indexPath: IndexPath) -> Bool {
        
        guard indexPath.row >= 0, indexPath.row < schedulesViewModel.count else {
            print("Can't delete schedule number: \(indexPath.row)")
            return false
        }
        
        schedulesViewModel.remove(at: indexPath.row)
        
        // Notify delegate
        if _delegate != nil {
            _delegate?.onScheduleModelUpdated()
        }
        
        return true
        
    }
    
}

class ScheduleViewModel {
    
    // Variables
    private var _model: ScheduleModel!
    
    // Functions
    init(model: ScheduleModel) {
        self._model = model
    }
    
    // Computed properties
    var beginDateString: String {
        return _model.dateBegin.toLocalDateString
    }
    
    var endDateString: String {
        return _model.dateEnd.toLocalDateString
    }
    
    var beginDateUTC: Date {
        get {
            return _model.dateBegin
        }
        set {
            _model.dateBegin = newValue
            _model.dateEnd = newValue.addingTimeInterval(.week)
        }
    }
    
}
