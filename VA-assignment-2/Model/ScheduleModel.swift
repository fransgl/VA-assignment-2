//
//  ScheduleModel.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

class ScheduleModel {
    
    // Variables
    var dateBegin: Date
    var dateEnd: Date
    
    // Functions
    init(dateBegin: Date, dateEnd: Date) {
        self.dateBegin = dateBegin
        self.dateEnd = dateEnd
    }
    
}
