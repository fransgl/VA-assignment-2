//
//  ScheduleTableViewCell.swift
//  VA-assignment-2
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    // Variables
    var viewModel: ScheduleViewModel!
    
    // Outlets
    @IBOutlet weak var labelBeginDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    

    // Functions
    func configure(with viewModel: ScheduleViewModel) {
        
        self.viewModel = viewModel
        
        labelBeginDate.text = viewModel.beginDateString
        labelEndDate.text = viewModel.endDateString
        
    }

}
