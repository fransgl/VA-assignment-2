//
//  OverviewViewController.swift
//  
//
//  Created by Frans Glorie on 08/02/2018.
//

import UIKit

class OverviewViewController: UIViewController, ScheduleDelegate {

    // Variables
    var viewModel: SchedulesViewModel!
    
    // Outlets
    @IBOutlet weak var tableViewSchedules: UITableView!
    
    // Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up MVVM chain
        viewModel = SchedulesViewModel(delegate: self)
        
        tableViewSchedules.dataSource = self
        tableViewSchedules.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let _segueIdentifier = segue.identifier else {
            print("Encountered a nil segue identifier. This is a programming error that needs to be fixed.")
            fatalError()
        }
        
        switch _segueIdentifier {
        case "segueAddSchedule":

            if let targetVC = segue.destination as? ScheduleViewController {
                let newScheduleModel = ScheduleModel(dateBegin: Date(), dateEnd: Date().addingTimeInterval(.week))
                let newScheduleViewModel = ScheduleViewModel(model: newScheduleModel)
                self.viewModel.schedulesViewModel.append(newScheduleViewModel)
                targetVC.viewModel = newScheduleViewModel
                targetVC.delegate = self
            } else {
                print("Segue \(_segueIdentifier) points to wrong VC class")
            }
        case "segueScheduleDetails":
            
            if let sendingCell = sender as? ScheduleTableViewCell {
                
                if let targetVC = segue.destination as? ScheduleViewController {
                    targetVC.viewModel = sendingCell.viewModel
                    targetVC.delegate = self
                } else {
                    print("Segue \(_segueIdentifier) points to wrong VC class")
                }
            } else {
                print("Sender is incorrect type")
            }
        default: print("No pre-defined segue: \(_segueIdentifier). This should never happen")
        }
    }
    
}

extension OverviewViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSchedules
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellSchedule", for: indexPath) as? ScheduleTableViewCell {
        
            cell.configure(with: viewModel.schedulesViewModel[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func onScheduleModelUpdated() {

        tableViewSchedules.reloadData()
    
    }
    
}

extension OverviewViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    // Handle swipt-to-delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            _ = viewModel.deleteSchedule(indexPath: indexPath)

        }
    }
    
}
