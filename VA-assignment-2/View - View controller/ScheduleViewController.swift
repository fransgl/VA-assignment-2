//
//  ScheduleViewController.swift
//  VA-assignment-1
//
//  Created by Frans Glorie on 07/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {

    var viewModel: ScheduleViewModel!
    var delegate: ScheduleDelegate?
    
    // Outlets
    @IBOutlet weak var labelBeginDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var datePickerBeginDate: UIDatePicker!
    
    // Actions
    @IBAction func pbClear(_ sender: UIBarButtonItem) {
        datePickerBeginDate.date = Date()
        updateLabels(using: datePickerBeginDate.date)
    }
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        updateLabels(using: sender.date)
    }
    @IBAction func pbSaveAndBack(_ sender: UIBarButtonItem) {
        
        // Save data
        viewModel.beginDateUTC = datePickerBeginDate.date
        
        // Notify calling view controller
        if delegate != nil {
            delegate?.onScheduleModelUpdated()
        }
        
        // Dismiss this vc
        dismiss(animated: true, completion: nil)
    }
    
    
    // Functions
    func updateLabels(using beginDate: Date) {
        
        // Update labels
        labelBeginDate.text = beginDate.toLocalDateString
        
        let timeInterval: TimeInterval = 7 * 24 * 3600
        labelEndDate.text = beginDate.addingTimeInterval(timeInterval).toLocalDateString
        
    }
    
    // Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup controls
        datePickerBeginDate.minimumDate = Date()
        
        // Update labels
        datePickerBeginDate.date = viewModel.beginDateUTC
        updateLabels(using: viewModel.beginDateUTC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
