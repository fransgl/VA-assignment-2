//
//  VA_assignment_2Tests.swift
//  VA-assignment-2Tests
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import XCTest
@testable import VA_assignment_2

class VA_assignment_2Tests: XCTestCase {
    
    // Test variables
    // 2018-02-11
    let testDate = Date(timeIntervalSince1970: 1518350400)
    var scheduleViewModel: ScheduleViewModel!
    var scheduleModel: ScheduleModel!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        scheduleModel = ScheduleModel(dateBegin: testDate, dateEnd: testDate)
        scheduleViewModel = ScheduleViewModel(model: scheduleModel)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDateStringFormatter() {
        
        XCTAssert(testDate.toLocalDateString == "11 February 2018")
        
    }
    
    func testEndDate() {
        
        scheduleViewModel.beginDateUTC = testDate
        XCTAssert(scheduleViewModel.endDateString == "18 February 2018")
        
    }
    
}
